import sys
import click
from aux_functions import *

@click.command()
def plot3D():
    plot_3D()

@click.command()
def plotGraph():
    plot_graph()

@click.group(invoke_without_command=True)
@click.option("--version", is_flag=True, default=False)
def main(version):
    if len(sys.argv) == 1:
        print("ldb tool")


main.add_command(plot3D)
main.add_command(plotGraph)

if __name__ == "__main__":
    main()
