# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 23:01:31 2021

@author: diogo
"""

import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plot
import numpy as np
import seaborn as sns
from sklearn.model_selection import TimeSeriesSplit

def new_feat(data):
    
    #Deteta higj load (>0.6)
    high_load = np.where(data['Load']>=0.6)
    data["high_load"]=0
    data.iloc[high_load[0], 3]=1
    
    crash=np.where(data['Falha']==1)
    
    request_t_1 = []
    request_t_1 = crash[0] - 1
    data['request t-1']=0
    data['request t-1']=data.iloc[request_t_1,0]
    data['request t-1'] = data['request t-1'].fillna(0)
        
    request_t_2 = []
    request_t_2 = crash[0] - 2
    data['request t-2']=0
    data['request t-2']=data.iloc[request_t_2,0]
    data['request t-2'] = data['request t-2'].fillna(0)
    
    request_t_3 = []
    request_t_3 = crash[0] - 3
    data['request t-3']=0
    data['request t-3']=data.iloc[request_t_3,0]
    data['request t-3'] = data['request t-3'].fillna(0)
    
    request_t_4 = []
    request_t_4 = crash[0] - 4
    data['request t-4']=0
    data['request t-4']=data.iloc[request_t_4,0]
    data['request t-4'] = data['request t-4'].fillna(0)
    
    return data
    
def balance(data):
    
    flag=0
    
    out=[]
    
    for i in range(1, 1800):
        
        if(i>=1790):
    
            break
        
        flag=0
        
        if(data.iloc[i, 2]!=1):
        
            for j in range(7):
            
                if(data.iloc[i+j+1, 2]==1):
                
                  #  print("encontrou crash no index: ", i)
                    flag=1
                    
                    break
            
            if(flag==0):
                
                #print("dropou: ", i)
                out.append(i)
    
    data.drop(out,inplace = True)
    #sns.relplot(x='Load', y='Requests', hue="Falha",data=data);
    
    return data
    
data = pd.read_csv("ACI21-22_Proj1IoTGatewayCrashDataset.csv", sep=',', decimal='.')

data = new_feat(data)
# data.drop('Requests',1)
# data.drop('high_load',1)

#cria data para validaçao (nao balanced)
data_val = data.iloc[np.arange(1800, 1899, 1),:]
y_val=data_val['Falha']
data_val.drop('Falha', 1)
x_val=data_val

#cria data para teste (nao balanced)
data_test = data.iloc[np.arange(1900, 1999, 1),:]
y_test=data_test['Falha']
data_test.drop('Falha', 1)
x_test=data_test

#retira sets de validaçao e teste da data
data.drop(np.arange(1900, 1999, 1),inplace = True)
data.drop(np.arange(1800, 1899, 1),inplace = True)

#balance e cria set de teste
data_bal=balance(data)
y_train = data_bal['Falha']
data_bal.drop('Falha', 1)
x_train = data_bal

model = MLPClassifier(hidden_layer_sizes=(6), random_state=10,shuffle = False, activation="relu")

model.fit(x_train, y_train)
        
y_pred_val = model.predict(x_val)
        
y_pred_test = model.predict(x_test)


print('############## VALIDATION ##############\n')
    
print("Accuracy: ",accuracy_score(y_val, y_pred_val))
        
print('Precision: ',precision_score(y_val, y_pred_val, average='macro'))
            
print('Recall: ',recall_score(y_val, y_pred_val, average='macro'))

print('Confusion matrix:',confusion_matrix(y_val, y_pred_val))
        
print('########################################\n')
        
print('############## TEST ##############\n')
        
print("Accuracy: ",accuracy_score(y_test, y_pred_test))
        
print('Precision: ',precision_score(y_test, y_pred_test, average='macro'))
            
print('Recall: ',recall_score(y_test, y_pred_test, average='macro'))
            
print('Confusion matrix:',confusion_matrix(y_test, y_pred_test))
        
print('########################################\n')
