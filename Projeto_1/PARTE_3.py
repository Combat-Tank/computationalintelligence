# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 15:29:44 2021

@author: diogo
"""
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from sklearn.metrics import confusion_matrix
import matplotlib.pyplot as plot
import numpy as np
import seaborn as sns
import warnings


data = pd.read_csv("ACI21-22_Proj1IoTGatewayCrashDataset.csv", sep=",", decimal=".")

y_data = data["Falha"]

x_data = data.drop("Falha", 1)

x_train, x_test, y_train, y_test = train_test_split(
    x_data, y_data, test_size=0.3, random_state=1
)

x_test, x_val, y_test, y_val = train_test_split(
    x_test, y_test, test_size=0.5, random_state=1
)

print("############## 1 LAYER ##############\n")
for i in range(1, 6):

    print("############## ", i, " NODE ###########\n")

    model = MLPClassifier(
        hidden_layer_sizes=(i), random_state=1, activation="relu"
    )  # 4,5 ou 4,5
    warnings.filterwarnings("ignore")
    model.fit(x_train, y_train)

    y_pred_val = model.predict(x_val)

    y_pred_test = model.predict(x_test)

    print("############## VALIDATION ##############\n")

    print("Accuracy: ", accuracy_score(y_val, y_pred_val))

    print("Precision: ", precision_score(y_val, y_pred_val, average="macro"))

    print("Recall: ", recall_score(y_val, y_pred_val, average="macro"))

    print("Confusion matrix:", confusion_matrix(y_val, y_pred_val))

    print("########################################\n")

    print("############## TEST ##############\n")

    print("Accuracy: ", accuracy_score(y_test, y_pred_test))

    print("Precision: ", precision_score(y_test, y_pred_test, average="macro"))

    print("Recall: ", recall_score(y_test, y_pred_test, average="macro"))

    print("Confusion matrix:", confusion_matrix(y_test, y_pred_test))

    print("########################################\n")


print("############## 2 LAYER ##############\n")

for i in range(1, 6):

    for j in range(1, 6):

        print("############## ", i, j, " NODE ###########\n")

        model = MLPClassifier(
            hidden_layer_sizes=(i, j), random_state=1, activation="relu"
        )  # 4,5 ou 4,5
        warnings.filterwarnings("ignore")
        model.fit(x_train, y_train)

        y_pred_val = model.predict(x_val)

        y_pred_test = model.predict(x_test)

        print("############## VALIDATION ##############\n")

        print("Accuracy: ", accuracy_score(y_val, y_pred_val))

        print("Precision: ", precision_score(y_val, y_pred_val, average="macro"))

        print("Recall: ", recall_score(y_val, y_pred_val, average="macro"))

        print("Confusion matrix:", confusion_matrix(y_val, y_pred_val))

        print("########################################\n")

        print("############## TEST ##############\n")

        print("Accuracy: ", accuracy_score(y_test, y_pred_test))

        print("Precision: ", precision_score(y_test, y_pred_test, average="macro"))

        print("Recall: ", recall_score(y_test, y_pred_test, average="macro"))

        print("Confusion matrix:", confusion_matrix(y_test, y_pred_test))

        print("########################################\n")

crash = np.where(data["Falha"] == 1)

falhas = data["Falha"]
crash = [x - 1 for x in crash]
falhas.iloc[crash] = 0.5

data["Falha"] = falhas

plot.figure()

plot.title("Requests vs Falha")

plot.plot(np.arange(0, 1999, 1), data["Requests"])

plot.plot(np.arange(0, 1999, 1), data["Falha"])

plot.figure()

plot.title("Load vs Falha")

plot.plot(np.arange(0, 1999, 1), data["Load"])

plot.plot(np.arange(0, 1999, 1), data["Falha"])

sns.relplot(x="Load", y="Requests", hue="Falha", data=data)
