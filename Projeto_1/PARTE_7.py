import skfuzzy as fuzz
import skfuzzy.control as ctrl
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import *
from matplotlib.font_manager import FontProperties
from aux_functions import *

#df = pd.read_csv('ACI21-22_Proj1IoTGatewayCrashDataset.csv')

df = pd.read_csv('ACI_Proj1_TestSet.csv', names=['Requests', 'Load', 'Falha'])

#The range for all parameters is the same
x = np.arange(0, 1, 0.01)

av_requests = ctrl.Antecedent(x, 'av_requests')
av_load = ctrl.Antecedent(x, 'av_load')
prob_crash = ctrl.Consequent(x, 'prob_crash')

av_load['low'] = fuzz.trapmf(x, [0.0, 0.0, 0.3, 0.4])
av_load['medium'] = fuzz.trapmf(x, [0.3, 0.4, 0.5, 0.65])
av_load['high'] = fuzz.trapmf(x, [0.5, 0.6, 1, 1])

av_requests['low'] = fuzz.trapmf(x, [0.0, 0.0, 0.35, 0.45])
av_requests['medium'] = fuzz.trapmf(x, [0.35, 0.4, 0.7, 0.8])
av_requests['high'] = fuzz.trapmf(x, [0.7, 0.8, 1, 1])

prob_crash['normal'] = fuzz.trapmf(x, [0, 0, 0.4, 0.6])
prob_crash['crash'] = fuzz.trapmf(x, [0.4, 0.6, 1.0, 1.0])

rule_1 = ctrl.Rule(antecedent = av_load['low'] , consequent = prob_crash['normal'])

rule_2 = ctrl.Rule(antecedent = av_load['medium'], consequent = prob_crash['normal'])

rule_3 = ctrl.Rule(antecedent = av_load['high'] & av_requests['low'], consequent = prob_crash['normal'])
rule_4 = ctrl.Rule(antecedent= av_load['high'] & av_requests['medium'], consequent = prob_crash['crash'])
rule_5 = ctrl.Rule(antecedent = av_load['high'] & av_requests['high'], consequent = prob_crash['crash'])

system = ctrl.ControlSystem([rule_1, rule_2, rule_3, rule_4, rule_5])

simulator = ctrl.ControlSystemSimulation(system)

output_list = []
moving_av = df['Requests'].rolling(min_periods=1, window=5).mean()

for i in range(len(df)):
    simulator.input['av_load'] = df.at[i, 'Load']
    simulator.input['av_requests'] = moving_av.at[i]
    simulator.compute()
    if simulator.output['prob_crash'] >= 0.6:
        output_list.append(1)
    else:
        output_list.append(0)

prob_crash.view(sim = simulator)

falha_count = df['Falha'].value_counts().iloc[1]

print(confusion_matrix(df['Falha'], output_list))