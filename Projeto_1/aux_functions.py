import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv('ACI21-22_Proj1IoTGatewayCrashDataset.csv')

def plot_graph():
    plt.plot(df['Requests'].rolling(min_periods=1, window=5).sum()/5)
    #plt.plot(df['Requests'])
    plt.plot(df['Load'])
    plt.plot(df['Falha'])
    #plt.legend(['Number of Requests','Load','CRASH'])
    #plt.legend(['Medium of 5 requests B4','Number of Requests','Load','CRASH'])
    plt.legend(['Medium of 5 requests B4','Load','CRASH'])
    plt.show()
    return

def plot_3D():
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    #ax.scatter(df['Requests'],df['Load'],df['Falha'])
    ax.scatter(df['Requests'].rolling(min_periods=1, window=5).mean(),df['Load'],df['Falha'])
    #ax.set_xlabel("Requests")
    ax.set_xlabel("Medium of 5 requests B4")
    ax.set_ylabel("Load")
    ax.set_zlabel("Falha")
    plt.show()
    return

