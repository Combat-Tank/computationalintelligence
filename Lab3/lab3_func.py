import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt

def find_outliers_index(data):
    k = 5
    outliers_index = []
    for i in range(1,len(data.columns)-1):
        outlier = np.where(np.abs(data.iloc[:,i] - data.iloc[:,i].mean()) > k * data.iloc[:,i].std())[0].tolist()
        outliers_index.append(outlier[0])

    return outliers_index

def remove_outliers(data):
    outliers = find_outliers_index(data)
    for j in range(1,len(data.columns)):
        for i in outliers:
            data.iloc[:,j].drop(i,inplace = False)
    
    return