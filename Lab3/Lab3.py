# -*- coding: utf-8 -*-
"""
Created on  September 07 2021

@author: Manuel Gil Mata Ribeiro (@Combat-Tank in git)

Lab 3 Computacional Inteligence

Do Lab3_21.py ex_"exercise number"
"""

import sys
import click
from lab3_func import remove_outliers
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import datetime as dt

""" plt.plot(df)
plt.xlabel('time')
plt.show() """

#Exercise 1
@click.command()
def ex1():
    df = pd.read_csv('EURUSD_Daily_Ask_2018.12.31_2019.10.05v2.csv', sep=";",decimal=",")

    df.plot()
    plt.show()
    
    remove_outliers(df)

    for i in range(1,len(df.columns), 1):
        plt.plot(df.iloc[:,0], df.iloc[:,i], 'ro', markersize = 1.2)
        plt.title(df.columns[i])
        plt.show()


@click.group(invoke_without_command=True)
@click.option("--version", is_flag=True, default=False)
def main(version):
    if len(sys.argv) == 1:
        print("Lab3 Tool")

main.add_command(ex1)

if __name__ == "__main__":
    main()
