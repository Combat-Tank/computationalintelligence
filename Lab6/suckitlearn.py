import skfuzzy as fuzz
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontProperties

core_temp = np.arange(101)
clck_freq = np.arange(0,4,0.1)
fan_speed = np.arange(6001)

core_lo = fuzz.trimf(core_temp, [0,0,50])
core_md = fuzz.trimf(core_temp, [30,50,70])
core_hi = fuzz.trimf(core_temp, [50,100,100])

clck_lo = fuzz.trimf(clck_freq, [0,0,1.5])
clck_md = fuzz.trimf(clck_freq, [0.5,2,3.5])
clck_hi = fuzz.trimf(clck_freq, [2.5,4,4])

fan_lo = fuzz.trimf(fan_speed, [0,0,3500])
fan_hi = fuzz.trimf(fan_speed, [2500,6000,6000])

fig, (ax0, ax1, ax2) = plt.subplots(nrows=3, figsize=(8, 9))

ax0.plot(core_temp, core_lo, 'b', linewidth=1.5, label='Cold')
ax0.plot(core_temp, core_md, 'g', linewidth=1.5, label='Ambient')
ax0.plot(core_temp, core_hi, 'r', linewidth=1.5, label='HOT')
ax0.set_title('Core Temperature')
ax0.legend()

ax1.plot(clck_freq, clck_lo, 'b', linewidth=1.5, label='Poor')
ax1.plot(clck_freq, clck_md, 'g', linewidth=1.5, label='Acceptable')
ax1.plot(clck_freq, clck_hi, 'r', linewidth=1.5, label='Amazing')
ax1.set_title('Clock Frequency')
ax1.legend()

ax2.plot(fan_speed, fan_lo, 'b', linewidth=1.5, label='Low')
ax2.plot(fan_speed, fan_hi, 'r', linewidth=1.5, label='High')
ax2.set_title('Fan Speeeeeeeeeeeed')
ax2.legend()

for ax in (ax0, ax1, ax2):
    ax.spines['top'].set_visible(False)
    ax.spines['right'].set_visible(False)
    ax.get_xaxis().tick_bottom()
    ax.get_yaxis().tick_left()

plt.tight_layout()

plt.show()

core_level_lo = fuzz.interp_membership(core_temp, core_lo, 40)
core_level_md = fuzz.interp_membership(core_temp, core_md, 40)
core_level_hi = fuzz.interp_membership(core_temp, core_hi, 40)

clck_speed_lo = fuzz.interp_membership(clck_freq, clck_lo, 3)
clck_speed_md = fuzz.interp_membership(clck_freq, clck_md, 3)
clck_speed_hi = fuzz.interp_membership(clck_freq, clck_hi, 3)

fan_speed_lo = fuzz.interp_membership(fan_speed, fan_lo, 6.5)
fan_speed_hi = fuzz.interp_membership(fan_speed, fan_hi, 6.5)