# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 22:01:50 2021

@author: diogo
"""
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

data = pd.read_csv("CitiesXY.csv", sep=',', decimal='.')
low=data
high=data

plt.scatter(data['x'], data['y'])

rem1=np.where(data['x']<400)
rem2=np.where(data['x']>=400)

low=low.drop(rem2[0])
high=high.drop(rem1[0])

esquerda=low.sort_values(by='y')
direita=high.sort_values(by='y', ascending=False)
frames=[esquerda, direita]
result = pd.concat(frames, keys=["city", "x", "y"])