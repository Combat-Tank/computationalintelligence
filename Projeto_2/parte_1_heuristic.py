# -*- coding: utf-8 -*-
"""
Created on Wed Nov  3 10:42:31 2021

@author: diogo
"""
import pandas as pd
import random
import numpy as np
import matplotlib.pyplot as plt

from deap import base
from deap import creator
from deap import tools
from random import sample


best = []
N_GENERATIONS=70

# =============================================================================
# FUNÇAO DE CUSTO
# =============================================================================
def eval(individual):
        total = 0
        for i in range(maximo-1):
            total += data[individual[i],individual[i+1]]
        
        return total,

# =============================================================================
# FUNÇAO QUE CRIA SOLUÇAO HEURISTICA
# =============================================================================
def generate_heuristic(MAXIMO):
    
    data_xy = pd.read_csv("CitiesXY.csv", sep=',', decimal='.')
    
    remove_city = np.where(data_xy['City']>=MAXIMO)
    
    data_xy=data_xy.drop(remove_city[0])
    
    low=data_xy
    high=data_xy
    
    plt.scatter(data_xy['x'], data_xy['y'])
    
    rem1=np.where(data_xy['x']<400)
    rem2=np.where(data_xy['x']>=400)
    
    low=low.drop(rem2[0])
    high=high.drop(rem1[0])
    
    esquerda=low.sort_values(by='y')
    direita=high.sort_values(by='y', ascending=False)
    frames=[esquerda, direita]
    result = pd.concat(frames, keys=["city", "x", "y"])
    
    lista=result['City'].to_numpy()
    
    return lista

while True:
    
    global maximo
    global label
    
    label=[]
    
# =============================================================================
#     RECEBE INPUT DE MODO E NUMERO DE CIDADES
# =============================================================================
    print("\n1 - Distancia de carro\n2 - Preço de carro\n3 - Distancia de aviao\n4 - Preço de aviao\n5 - Sair\n")
    num = input("Numero: ")
    
    if num.isdigit():
        num=int(num)
        
        if(num==1):
            data = pd.read_csv("CityDistCar.csv", sep=',', decimal='.')
            data = data.drop('Distances of Cities by Car (min)', 1)
            label='Distância do percurso (min)'
            maximo = input("Numero de cidades: ")
            maximo=int(maximo)
    
            
        elif(num==2):
            data = pd.read_csv("CityCostCar.csv", sep=',', decimal='.')
            data = data.drop('Cost of Cities by Car (€)', 1)
            label='Custo do percurso (€)'
            maximo = input("Numero de cidades: ")
            maximo=int(maximo)
    
        
        elif(num==3):
            data = pd.read_csv("CityDistPlane.csv", sep=',', decimal='.')
            data = data.drop('Distances of Cities by Flight (in min)', 1)
            label='Distância do percurso (min)'
            maximo = input("Numero de cidades: ")
            maximo=int(maximo)
    
            
        elif(num==4):
            data = pd.read_csv("CityCostPlane.csv", sep=',', decimal='.')
            data = data.drop('Cost of Cities by Flight (€)', 1)
            label='Custo do percurso (€)'
            maximo = input("Numero de cidades: ")
            maximo=int(maximo)
        
        elif(num==5):
            break
    
    else:
        print("Nao valido\n")
      
    
    data = data.to_numpy()
    print(maximo)
    
# =============================================================================
#     CRIA LISTA DE CIDADES
# =============================================================================
    list_city = np.zeros(maximo)    
    for i in range(maximo):
        list_city[i] = i
    
    toolbox = base.Toolbox()
    
# =============================================================================
#     CRIA FERRAMENTAS DE EVOLUÇAO
# =============================================================================
    creator.create("FitnessMin", base.Fitness, weights=(-1.0,))
    creator.create("Individual", list, fitness=creator.FitnessMin)
    
    toolbox.register("indices", random.sample, range(maximo),maximo)
    toolbox.register("individual", tools.initIterate, creator.Individual, 
        toolbox.indices)
    toolbox.register("population", tools.initRepeat, list, toolbox.individual)
    toolbox.register("evaluate", eval)
    toolbox.register("mate", tools.cxOrdered)
    toolbox.register("mutate", tools.mutShuffleIndexes, indpb=0.05)
    toolbox.register("select", tools.selTournament, tournsize=3)
        
# =============================================================================
#     LOOP DE 30 RANDOM SEEDS
# =============================================================================
    best_seed=1000000 
    best_path=[]
    resultados = np.zeros(30)    
    for j in range(30):
        
        total=0
        random.seed(5*j)
        buffer=[]
        # create an initial population of 300 individuals (where
        # each individual is a list of integers)
        pop = toolbox.population(n=250)
        
        lista = generate_heuristic(maximo)
        ind=creator.Individual(lista)
        pop.append(ind)
        # CXPB  is the probability with which two individuals
        #       are crossed
        #
        # MUTPB is the probability for mutating an individual
        CXPB, MUTPB = 0.5, 0.2
        
        print("Start of evolution %i" % j)
        
        # Evaluate the entire population
        fitnesses = list(map(toolbox.evaluate, pop))
        for ind, fit in zip(pop, fitnesses):
            ind.fitness.values = fit
        
        # print("  Evaluated %i individuals" % len(pop))
        
        # Extracting all the fitnesses of 
        fits = [ind.fitness.values[0] for ind in pop]
        
        # Variable keeping track of the number of generations
        
        g = 0
        
        # Begin the evolution
        while  g < N_GENERATIONS:
            # A new generation
            g = g + 1
            # print("-- Generation %i --" % g)
            
            # Select the next generation individuals
            offspring = toolbox.select(pop, len(pop))
            # Clone the selected individuals
            offspring = list(map(toolbox.clone, offspring))
        
            # Apply crossover and mutation on the offspring
            for child1, child2 in zip(offspring[::2], offspring[1::2]):
        
                # cross two individuals with probability CXPB
                if random.random() < CXPB:
                    toolbox.mate(child1, child2)
        
                    # fitness values of the children
                    # must be recalculated later
                    del child1.fitness.values
                    del child2.fitness.values
        
            for mutant in offspring:
        
                # mutate an individual with probability MUTPB
                if random.random() < MUTPB:
                    toolbox.mutate(mutant)
                    del mutant.fitness.values
        
            # Evaluate the individuals with an invalid fitness
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = map(toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            total+=len(invalid_ind)
            # print("  Evaluated %i individuals" % len(invalid_ind))
            
            # The population is entirely replaced by the offspring
            pop[:] = offspring
            
            # Gather all the fitnesses in one list and print the stats
            fits = [ind.fitness.values[0] for ind in pop]
            
            length = len(pop)
            mean = sum(fits) / length
            sum2 = sum(x*x for x in fits)
            std = abs(sum2 / length - mean**2)**0.5
            
            # print("  Min %s" % min(fits))
            # print("  Max %s" % max(fits))
            # print("  Avg %s" % mean)
            # print("  Std %s" % std)
        
            buffer.append(sum(fits) / length)
            
        print("-- End of (successful) evolution --")
        
        best_ind = tools.selBest(pop, 1)[0]
        
        best=np.append(best, best_ind.fitness.values[0])
        print('Evaluated a total of %i individuals\n' % total)
        print("Best individual is %s, %s\n" % (best_ind, best_ind.fitness.values[0]))
        
        if(best_ind.fitness.values[0]<best_seed):
            best_path=[]
            best_path.append(buffer)
        
        resultados[j] = best_ind.fitness.values[0]
        print(resultados)
    
    min_index = np.where(best == np.amin(best))
    
    media=resultados.mean()
    
    print('\nMEDIA = ', media)
    
    std=np.std(resultados)
    print('\nSTD = ', std)
    int_array = best_path[0].astype(int)
    plt.plot(best_path[0])
    plt.xlabel('Número de Gerações')
    plt.ylabel(label)
    plt.show()