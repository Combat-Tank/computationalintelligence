import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

#exercise 4

df = pd.read_csv("DCOILBRENTEUv2.csv")

norm = []
z_score = []

cols = list(df['DCOILBRENTEU'])
maxv = max(cols)
minv = min(cols)

for x in cols:
	local_norm = (x-minv)/(maxv-minv)
	norm.append(local_norm)
	z_score.append((local_norm - np.mean(cols))/np.std(cols))

df['norm'] = norm
df['z-score'] = z_score

plt.figure()
plt.subplot(211)
plt.plot(norm)

plt.subplot(212)
plt.plot(z_score, 'r--')

sma = df['norm'].rolling(window = 50).mean()

plt.figure()
plt.plot(sma)
plt.show()